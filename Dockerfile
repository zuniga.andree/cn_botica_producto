FROM openjdk:11
ADD build/libs/*.jar cn_botica_producto-app.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "cn_botica_producto-app.jar"]